function initFixedNav() {
    var e = $(".navbar"), r = $(".nav-bg");
    $(window).on("scroll", function () {
        $(this).scrollTop() > 0 ? (e.removeClass("navbar-top").addClass("navbar-fix"), r.removeClass("top-nav").addClass("fix-nav")) : $(this).scrollTop() <= 0 && (e.removeClass("navbar-fix").addClass("navbar-top"), r.removeClass("fix-nav").addClass("top-nav"))
    })
}

function initScrollNav() {
    $(".smooth-nav").onePageNav({scrollSpeed: 2e3})
}

function InitScroll() {
    $(".smooth").smoothScroll({speed: 2e3})
}

function initParallaxTech() {
    $(".technologies").parallax("50%", .2)
}

function initParallaxCTA() {
    $(".cta-parallax-1").parallax("50%", .2), $(".cta-parallax-2").parallax("50%", .1)
}

function initAOS() {
    AOS.init({
        duration: 700, delay: 300, once: !0, disable: function () {
            return window.innerWidth < 420
        }
    })
}

function initSlider() {
    var e = $(".flexslider"), r = $(".flex-caption");
    e.flexslider({
        animation: "fade",
        slideshowSpeed: 7e3,
        animationSpeed: 2e3,
        controlNav: !1,
        prevText: "",
        nextText: "",
        start: function () {
        },
        before: function () {
            captionMoveOut()
        },
        after: function () {
            captionMoveIn()
        }
    }), r.hide(), r.fadeIn(700)
}

function captionMoveIn() {
    $(window).width() >= 768 && $(".flex-caption").animate({left: "7.5rem"}, 0).fadeIn(700)
}

function captionMoveOut() {
    $(window).width() >= 768 && $(".flex-caption").animate({left: "-100%"}, 900).fadeOut("normal")
}

// function initPopup() {
//     $(".buy-button").magnificPopup({type: "inline", midClick: !0, removalDelay: 300, mainClass: "mfp-fade"})
// }

function initVideo() {
    $(".popup-video").magnificPopup({
        disableOn: 700,
        type: "iframe",
        mainClass: "mfp-fade",
        removalDelay: 160,
        preloader: !1,
        fixedContentPos: !1
    })
}

function initVideoBg() {
    device.tablet() || device.mobile() ? $("#video").addClass("video-mobile-screen") : $("#video").YTPlayer({
        fitToBackground: !0,
        videoId: "mEWSuGgwdRk",
        playerVars: {autoplay: 1, showinfo: 0, branding: 0}
    })
}

function validateEmail(e) {
    return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(e)
}

function initOrder() {
    var e = $("#order-form"), r = $("#order-button"), a = $("#order-f-name"), o = $("#order-l-name"),
        n = $("#order-email"), i = $("#order-address"), t = $("#order-city"), l = $("#order-country"),
        s = $("#order-state"), d = $("#order-zip"), c = $("#order-phone");
    e.submit(function () {
        return !1
    }), r.on("click", function () {
        var f = a.val().length;
        f < 1 ? a.addClass("order-field-error") : f >= 1 && a.removeClass("order-field-error");
        var v = o.val().length;
        v < 1 ? o.addClass("order-field-error") : v >= 1 && o.removeClass("order-field-error");
        var p = validateEmail(n.val());
        !1 === p ? n.addClass("order-field-error") : !0 === p && n.removeClass("order-field-error");
        var u = i.val().length;
        u < 1 ? i.addClass("order-field-error") : u >= 1 && i.removeClass("order-field-error");
        var h = t.val().length;
        h < 1 ? t.addClass("order-field-error") : h >= 1 && t.removeClass("order-field-error");
        var m = $("#order-country option:selected").val();
        0 == m ? l.addClass("order-field-error") : 0 != m && l.removeClass("order-field-error");
        var w = s.val().length;
        w < 1 ? s.addClass("order-field-error") : w >= 1 && s.removeClass("order-field-error");
        var x = d.val().length;
        x < 1 ? d.addClass("order-field-error") : x >= 1 && d.removeClass("order-field-error");
        var g = c.val().length;
        g < 1 ? c.addClass("order-field-error") : g >= 1 && c.removeClass("order-field-error"), f >= 1 && v >= 1 && !0 === p && u >= 1 && h >= 1 && 0 != m && w >= 1 && x >= 1 && g >= 1 && (r.replaceWith("<span class='order-send'>send...</span>"), $.ajax({
            type: "POST",
            url: "order.php",
            data: e.serialize(),
            success: function (r) {
                "true" === r && e.fadeOut("fast", function () {
                    $(this).before("<p>Thank you for your order!</p><span class='order-send'><svg class='check' width='44px' height='22px' viewBox='0 0 59 42' version='1.1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' xmlns:sketch='http://www.bohemiancoding.com/sketch/ns'><g stroke='none' stroke-width='1' fill='none' fill-rule='evenodd' sketch:type='MSPage'><path d='M4.5,20.5 L21.0302753,37.0302753 L54.5,4.5' id='line' stroke='#fff' stroke-width='4' stroke-linecap='round' stroke-linejoin='round' sketch:type='MSShapeGroup'></path></g></svg></span>")
                })
            }
        }))
    })
}

function initMail() {
    var e = $("#newsletter-form"), r = $("#newsletter-submit"), a = $("#newsletter-email"), o = $(".nf-error-message");
    e.submit(function () {
        return !1
    }), r.on("click", function () {
        var n = validateEmail(a.val());
        !1 === n ? o.addClass("nf-error") : !0 === n && o.removeClass("nf-error"), !0 === n && (r.replaceWith("<span class='nf-send'>send...</span>"), $.ajax({
            type: "POST",
            url: "newsletter.php",
            data: e.serialize(),
            success: function (e) {
                "true" === e && $(".nf-send").fadeOut("fast", function () {
                    $(this).before("<span class='nf-success'><svg class='check' width='44px' height='22px' viewBox='0 0 59 42' version='1.1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' xmlns:sketch='http://www.bohemiancoding.com/sketch/ns'><g stroke='none' stroke-width='1' fill='none' fill-rule='evenodd' sketch:type='MSPage'><path d='M4.5,20.5 L21.0302753,37.0302753 L54.5,4.5' id='line' stroke='#fff' stroke-width='4' stroke-linecap='round' stroke-linejoin='round' sketch:type='MSShapeGroup'></path></g></svg></span>")
                })
            }
        }))
    })
}

jQuery(function (e) {
    "use strict";
    e(window).on("load", function () {
        e("#page-preloader").delay(1e3).fadeOut("slow"), e(".title-text").addClass("animation-right"), e(".title-product").addClass("animation-left")
    }), e("#menu-navbar").on("click", function () {
        e(".navbar-collapse").removeClass("in"), e(".nav-icon").toggleClass("open")
    }), e(".nav-icon").on("click", function () {
        e(this).toggleClass("open")
    }), initFixedNav(), initScrollNav(), InitScroll(), initParallaxTech(), initParallaxCTA(), initAOS(), initVideo(), initSlider(), initPopup(), initVideoBg(), initOrder(), initMail()
}), $("a[rel=popover]").popover().click(function (e) {
    e.preventDefault();
    var r = $(this).attr("data-easein");
    "shake" == r ? $(this).next().velocity("callout." + r) : "pulse" == r ? $(this).next().velocity("callout." + r) : "tada" == r ? $(this).next().velocity("callout." + r) : "flash" == r ? $(this).next().velocity("callout." + r) : "bounce" == r ? $(this).next().velocity("callout." + r) : "swing" == r ? $(this).next().velocity("callout." + r) : $(this).next().velocity("transition." + r)
});