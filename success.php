<?php
if (!empty($_GET['tid'] && !empty($_GET['product']))) {
    $GET = filter_var_array($_GET, FILTER_SANITIZE_STRING);


    $vorname = $GET['vorname'];
    $nachname = $GET['nachname'];
    $email = $GET['email'];
    $stadt = $GET['stadt'];
    $adresse = $GET['adresse'];
    $postleitzahl = $GET['postleitzahl'];
    $quantityFlasica = $GET['quantityFlasica'];
    $quantitySpray = $GET['quantitySpray'];
    $flasicaPrice = 2495;
    $sprayPrice = 2995;
    $headers = "Content-Type: text; charset=UTF-8";


    $tid = $GET['tid'];
    $product = $GET['product'];

    $ostatak = $GET['ostatak'];
    $kolicnik = $GET['kolicnik'];
    $sprayCenaTotal = $GET['sprayCenaTotal'];
    $flasicaCenaTotal = $GET['flasicaCenaTotal'];
    $price = $GET['price'];
} else {
    header('Location: index.html');
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Thank You</title>
</head>
<body>
<div class="container mt-4">
    <h2>Hallo <?php echo $vorname ?></h2>
    <hr>
    <h5>Tolle Neuigkeiten!</h5>

    <p>Deine Bestellung ist jetzt bestätigt und wird in den nächsten 2-3 Arbeitstagen versendet.</p>

    <p>Bestelnummer <?php echo $tid ?></p>

    <h5>Lieferadresse:</h5>

    <p style="padding-left: 2rem"><?php echo $vorname, " ", $nachname ?></p>
    <p style="padding-left: 2rem">Stadt: <?php echo $stadt ?></p>
    <p style="padding-left: 2rem">Adresse: <?php echo $adresse ?></p>
    <p style="padding-left: 2rem">Postleitzahl: <?php echo $postleitzahl ?></p>
    <p style="padding-left: 2rem">Email: <?php echo $email ?></p>

    <h5>Zahlungsmodalitaten</h5>

    <p style="padding-left: 2rem">C-O-C Tropfen 10ml x
        <b><?php echo $quantityFlasica, " - ", number_format($flasicaCenaTotal / 100, 2) ?> CHF</b></p>
    <p style="padding-left: 2rem">Immune To-Go Spray x
        <b><?php echo $quantitySpray, " - ", number_format($sprayCenaTotal / 100, 2) ?> CHF</b></p>
    <p style="padding-left: 2rem">gesamt Versandkosten: <b><?php echo number_format($price / 100, 2) ?> CHF</b></p>


    <!--    <p>Check your email for more info</p>-->
    <p><a href="index.html" class="btn btn-light mt-2">Go Back</a></p>
</div>
</body>
</html>