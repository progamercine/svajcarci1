<?php
require_once('stripe-php-7.28.1/init.php');
//
//
\Stripe\Stripe::setApiKey('sk_live_rJLG6tbwivIkl8Mw7zngm0ma00FLi2tmIb');
//\Stripe\Stripe::setApiKey('sk_test_X8jmdYgfVRRTX3VOIfBc8kKr00feKyybMM');


// Sanitize POST Array
$POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);

$vorname = $POST['vorname'];
$nachname = $POST['nachname'];
$email = $POST['email'];
$stadt = $POST['stadt'];
$adresse = $POST['adresse'];
$postleitzahl = $POST['postleitzahl'];
$flasica = $POST['flasica'];
$quantityFlasica = $POST['quantityFlasica'];
$spray = $POST['spray'];
$quantitySpray = $POST['quantitySpray'];
$flasicaPrice = 2995;
$sprayPrice = 2495;
$headers = "Content-Type: text; charset=UTF-8";

$to = 'v.kulundzic@ffsuisse.ch';
//$to = 'ilicmil4n@gmail.com';
//$to = 'marko.uljarevic@hotmail.com';
$subject = 'Order';
$body = "Vorname: $vorname\nNachname: $nachname\nE-Mail: $email\nStadt: $stadt\nPostleitzahl: $postleitzahl\nc-o-c Tropfen 10ml: $quantityFlasica\nImmune to-go Spray 8ml: $quantitySpray\n";
$from = 'MAIL ORDER';

if (mail($to, $subject, $body, $from)) {
    $result = true;
}


if ($quantitySpray >= 5) {
    $ostatak = $quantitySpray % 5;
    $kolicnik = floor($quantitySpray / 5);
    $sprayCenaTotal = $ostatak * $sprayPrice + $kolicnik * 10000;
} else {
    $sprayCenaTotal = $sprayPrice * $quantitySpray;
}

$flasicaCenaTotal = $flasicaPrice * $quantityFlasica;

$price = $sprayCenaTotal + $flasicaCenaTotal;
if ($price < 10000) {
    $price = $price + 890;
}

$token = $POST['stripeToken'];

echo $token;

// Create Customer In Stripe
$customer = \Stripe\Customer::create(array(
    "email" => $email,
    "source" => $token
));

// Charge Customer
$charge = \Stripe\Charge::create(array(
    "amount" => $price,
    "currency" => "chf",
    "description" => "Vidartis",
    "customer" => $customer->id
));

// Customer email
$subject = 'Order';
$body = "Hello $vorname\n\nTolle Neuigkeiten! :)\nDeine Bestellung ist jetzt bestätigt und wird in den nächsten 2-3 Arbeitstagen versendet.\n
Bestelnummer: $charge->id\n\nLieferadresse:\n        $vorname $nachname\n        Stadt: $stadt\n        Adresse: $adresse\n        Postleitzahl: $postleitzahl\n        $email\n\nZahlungsmodalitaten:\n        C-O-C Tropfen 10ml x $quantityFlasica " . number_format($flasicaCenaTotal / 100, 2) . " CHF\n        Immune To-Go Spray 8ml x $quantitySpray " . number_format($sprayCenaTotal / 100, 2) . " CHF\n        gesamt Versandkosten:" . number_format($price / 100, 2) . " CHF\n";
$from = 'C-O-C Shop';

if (mail($email, $subject, $body, $headers, $from)) {
    $result = true;
}

// Redirect to success
header('Location: success.php?tid=' . $charge->id . '&product=' . $charge->description . '&ostatak=' . $ostatak . '&kolicnik=' . $kolicnik . '&sprayCenaTotal=' . $sprayCenaTotal . '&flasicaCenaTotal=' . $flasicaCenaTotal . '&price=' . $price . '&stadt=' . $stadt . '&adresse=' . $adresse . '&postleitzahl=' . $postleitzahl . '&email=' . $email . '&vorname=' . $vorname . '&quantitySpray=' . $quantitySpray . '&quantityFlasica=' . $quantityFlasica . '&nachname=' . $nachname);
?>